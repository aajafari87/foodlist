import React, { Component } from 'react';
import ProductsList from './components/ProductsList.js';
import AddProduct from './components/AddProduct.js';
import EditProduct from './components/EditProduct.js';
import { BrowserRouter as Router, Route,BrowserRouter } from "react-router-dom";
import './assets/bootstrap.min.css';
import './assets/custom.css';
class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <div>
              <Route exact path="/" component={ProductsList} />
              <Route path="/add/" component={AddProduct} />
	          <Route path="/edit/:id" component={EditProduct} />
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
