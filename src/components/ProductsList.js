import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import * as firebase from 'firebase';
import ListItem from './ListItem.js';
var categories = require('../categories');
var fireBaseConfig = require('../Config');
firebase.initializeApp(fireBaseConfig);

class ProductsList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data:[],
			categories:[]
		};
		this.getCategory = this.getCategory.bind(this);
	}
	
	componentDidMount() {
		this.getData()
	}
	
	getData() {
		var self = this;
		var data = firebase.database().ref("/");
		data.on('value', list => {
			
			self.setState({
				data:list.val()
			});
		});
		
		this.setState({
			categories:categories
		})
		
	}
	
	getCategory(code) {
		var self = this;
		var name = '';
		this.state.categories.map(function(Item,i) {
			if(Item['code'] == code) {
				name = Item['title']
			}
		})
		
		return name;
	
	}
	
	data() {
		var objects = this.state.data;
		var lists = [];
		var self = this;
		
			if(objects != null) {
				
				Object.keys(objects).map(function(key, index) {
					lists.push(
						<ListItem
							key={index}
							ukey={key}
							key={index}
							data={objects[key]}
							categoryName={self.getCategory(objects[key]['category_code'])}
						/>
					)
				});
				
			}
		
			
		
		
		return lists;
		
		
	
	}
	
	render() {
		return (
			<div>
				<div className='container container-custom'>
					<h1 className='text-center'>Product List</h1>
					
					<Link to='/add' className='btn btn-success add-product-btn'>Add Product</Link>
					<div className="table-responsive">
						<table className="table table-striped">
							<thead>
							<tr>
								<th>Name</th>
								<th>Price</th>
								<th>Category</th>
								<th colSpan={2}>Availability</th>
							</tr>
							</thead>
							<tbody>
							
							{this.data()}
							
							</tbody>
						</table>
					</div>
				</div>
			</div>
		);
	}
}

export default ProductsList;
