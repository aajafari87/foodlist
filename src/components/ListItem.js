import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import * as firebase from 'firebase';
var config = {
	apiKey: "AIzaSyDuky1GNQPNPRzkRmQDmA_D9uOuOM2TRec",
	authDomain: "productlist-34428.firebaseapp.com",
	databaseURL: "https://productlist-34428.firebaseio.com",
	projectId: "productlist-34428",
	storageBucket: "productlist-34428.appspot.com",
	messagingSenderId: "151793158044"
};
class ListItem extends Component {
	constructor(props) {
		super(props);
	}
	
	componentDidMount() {
	
	}

	deleteItem(ukey) {
		firebase.database().ref('/').child(ukey).remove();
	}
	
	render() {

		return (
			<tr>
				<td>{this.props.data.name}</td>
				<td>{this.props.data.price}</td>
				<td>{this.props.categoryName}</td>
				<td>{this.props.data.availability == '0' ? 'Not Available' : 'Available'}</td>
				<td>
					<Link className='btn btn-success' to={`/edit/${this.props.ukey}`}>Edit</Link>
					{`  `}
					<a className='btn btn-danger' href={`#`} onClick={() => { if (window.confirm('Are you sure, you want to delete this product?')) this.deleteItem(this.props.ukey) } }>Delete</a>
				</td>

			</tr>
		);
	}
}

export default ListItem;
