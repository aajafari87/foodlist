import React, {Component} from 'react';
import * as firebase from 'firebase';
import {Link} from 'react-router-dom';
var data = require('../categories');

class EditProduct extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name:null,
			price:null,
			description:null,
			category:null,
			availability:null,
			categories:null,
			status:null,
			message:null
		};
		this.changeInputs = this.changeInputs.bind(this);
		this.submit = this.submit.bind(this);
	}
	
	
	componentDidMount() {
		
		var self = this;
		var row = firebase.database().ref(`/${this.props.match.params.id}`);
		
		row.on('value', dataRow => {
			
			var data = dataRow.val();
			self.setState({
				name: data.name,
				price: data.price,
				description: data.description,
				category: data.category_code,
				availability: data.availability
			});
			
		});
	
	}
	
	getData(ukey) {
	
		console.log(ukey);
	
	}
	
	submit() {
		var self = this;
		firebase.database().ref(`/${this.props.match.params.id}`).set({
			"availability" : this.state.availability,
			"category_code" : this.state.category,
			"description" : this.state.description,
			"name" : this.state.name,
			"price" : parseInt(this.state.price)
		}).then((data)=>{
			
			self.setState({
				status:200,
				message:'product changed'
			})
			
		}).catch((error)=>{
			self.setState({
				status:500,
				message:'internal error'
			})
		})
	}
	
	categoryItems() {
		var self = this;
		var options = data.map(function(Item,i) {
			return <option value={Item.code} key={i} selected={self.state.category == Item.code ? 'selected' : ''}>{Item.title}</option>
		})
		
		return options
		
	}
	
	changeInputs(event) {
		var name = event.target.name;
		var value = event.target.value;
		this.setState({
			[name]:value
		})
	
	}
	
	render() {
		
		return (
			<div>
				<div className='container container-custom'>
					<h1 className='text-center'>Edit Product</h1>
					<Link to={'/'} className={'btn btn-primary add-product-btn'} >Back to Product List</Link>
					<div style={this.state.status == null ? {display:'none'} : {display:'block'}} class={`alert ${this.state.status == 200 ? 'alert-success' : 'alert-danger'}`} role="alert">
						{this.state.message}
					</div>
					<div className='form-container'>
						<div className="form-group row">
							<label
								className="col-sm-2 col-form-label">Name</label>
							<div className="col-sm-10">
								<input value={this.state.name} onChange={this.changeInputs} name='name' type="text" className="form-control"/>
							</div>
						</div>
						<div className="form-group row">
							<label
								className="col-sm-2 col-form-label">Price</label>
							<div className="col-sm-10">
								<input value={this.state.price}  onChange={this.changeInputs} name='price' type="number" className="form-control"/>
							</div>
						</div>
						<div className="form-group row">
							<label
								className="col-sm-2 col-form-label">Description</label>
							<div className="col-sm-10">
								<textarea onChange={this.changeInputs} name='description' type="text"
								          className="form-control" value={this.state.description} />
							</div>
						</div>
						<div className="form-group row">
							<label
								className="col-sm-2 col-form-label">Category</label>
							<div className="col-sm-10">
								<select name='category' onChange={this.changeInputs} className="custom-select">
									{this.categoryItems()}
								</select>
							</div>
						</div>
						<div className="form-group row">
							<label
								className="col-sm-2 col-form-label">Availability</label>
							<div className="col-sm-10">
								<div className="form-check form-check-inline">
									<div>
										<div className="form-check form-check-inline">
											<input
												className="form-check-input"
												type="radio"
												name="availability"
												id="not-available"
												value='0'
												checked={this.state.availability === '0' || this.state.availability == null}
												onChange={this.changeInputs}
											/>
											<label className="form-check-label" htmlFor="not-available">
												Not Available
											</label>
										</div>
										<div className="form-check form-check-inline">
											<input
												className="form-check-input"
												type="radio"
												name="availability"
												id="available"
												value='1'
												checked={this.state.availability === '1'}
												onChange={this.changeInputs}
											/>
											<label className="form-check-label" htmlFor="available">
												Available
											</label>
										</div>
									</div>
									
									
								</div>
							</div>
						</div>
						<div className="form-group">
							<button onClick={this.submit} type="button" className="float-right btn btn-success">Submit</button>
						</div>
					
					
					</div>
				
				
				</div>
			</div>
		);
	}
}

export default EditProduct;
